import os

from dotenv.main import find_dotenv, load_dotenv

load_dotenv(find_dotenv())

GITHUB_TOKEN = os.environ.get('GITHUB_TOKEN')
