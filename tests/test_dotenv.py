import settings


def test_github_token():
    assert settings.GITHUB_TOKEN, 'GitHub token can not be empty'
