from github.Label import Label

from github_py_api_example.core import is_bug_label


def test_gradle_labels():
    """
    owner: gradle
    repository_name: gradle
    """
    assert is_bug_label('a:bug')


def test_freecodecamp_labels():
    """
    owner: freeCodeCamp
    repository_name: freeCodeCamp
    """
    assert is_bug_label('bug')


def test_facebook_react_labels():
    """
    owner: facebook
    repository_name: react
    """
    assert is_bug_label('Type:Bug')


def test_tensorflow_labels():
    """
    owner: tensorflow
    repository_name: tensorflow
    """
    assert is_bug_label('type:bug/performance')


def test_vuejs_labels():
    """
    owner: vuejs
    repository_name: vue
    """
    assert is_bug_label('bug')


def test_angularjs_labels():
    """
    owner: angular
    repository_name: angular.js
    """
    assert is_bug_label('type:bug')
